#!/bin/bash
set -uo pipefail
# This script extends the swap if needed
# Based on https://bogdancornianu.com/change-swap-size-in-ubuntu/

# This function converts the size argument in bytes to Gib
function convert_bytes_to_gb
{
    local readonly size_in_bytes=$1
    let size_in_gb=${size_in_bytes}/1024/1024/1024
    echo ${size_in_gb}
}

# This function returns the size of the ram in GiB.
# Instead of grepping /proc/meminfo uses the swapon command
# so that the output is always in bytes
function get_swap_size
{
    local readonly swap_size=$(swapon --show --bytes --noheadings|gawk '{ print $3 }')
    echo $(convert_bytes_to_gb ${swap_size})
}

# This function returns the number of cores (hyperthreads)
function get_core_number
{
   local readonly core_nb=$(lscpu -p=CPU|tail -n 1)
   let res=${core_nb}+1  # The core numbering starts at 0
   echo ${res}
}

# This function computes and returns the required swap size (in GiB)
# It takes in arguments the number of cores
function compute_required_swap_size
{
    local readonly core_nb=$1
    let res=${core_nb}*4
    echo ${res}
}

# Print a message to stderr and exit with return code in argument
# if the preceeding command has failed.
# Otherwise print [OK] to stdout
function handle_rc
{
    if [[ $? != 0 ]]
    then
        echo "Error!" >&2
        exit $1
    else
        echo "[OK]"
    fi
}

# This function resizes the swap according to the argument passed in (bytes)
function resize_swap
{
    local readonly new_size_in_gb=$1

    if [[ "${EUID}" != "0" ]]
    then
        echo "Turning off all swap processes..."
        sudo swapoff -a
        handle_rc 1

        echo "Creating a swap file (/swapfile) with the good size..."
        sudo dd if=/dev/zero of=/swapfile bs=1G count=${new_size_in_gb}
        handle_rc 2

        echo "Changing permission on /swapfile"
        sudo chmod 600 /swapfile
        handle_rc 3

        echo "Making this file usable as swap..."
        sudo mkswap /swapfile
        handle_rc 4

        echo "Activating the swap..."
        sudo swapon /swapfile
        handle_rc 5

        echo "Remembering new swap location for futures boot..."
        echo "/swapfile none swap sw 0 0" | sudo tee -a /etc/fstab
        handle_rc 6

        echo "Swap resized successfully!"
    fi
}

# Install the environment-modules package that gives access to
# the command module
function install_module_cmd
{
    if [[ "${EUID}" != "0" ]]
    then
        echo "Installing environment-modules.."
        sudo apt-get install environment-modules
        handle_rc 7
    fi
}

function main
{
    local readonly swap_size=$(get_swap_size)
    local readonly core_nb=$(get_core_number)
    local readonly rqrd_swap_size=$(compute_required_swap_size ${core_nb})

    echo "Swap size is : ${swap_size} GiB" 
    echo "Cores number is : ${core_nb}" 
    echo "Required swap size is : ${rqrd_swap_size} GiB"

    if [[ ${swap_size} -lt ${rqrd_swap_size} ]]
    then
        echo "Not enough swap!"
        resize_swap ${rqrd_swap_size}
    else
        echo "Enough swap!"
    fi

    install_module_cmd
}

main
