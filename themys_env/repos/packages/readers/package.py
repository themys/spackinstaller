# -*- coding: utf-8 -*-

# Copyright 2020 Comissariat à l'Energie Atomique
#
# This is a template package file for Spack.
# You can install this package by typing:
#
#   spack install readers
#
# You can edit this file again by typing:
#
#   spack edit readers
#
# See the Spack documentation for more information on packaging.

from spack.package import *

import glob


class Readers(CMakePackage):
    """
    This code was developed by Military Applications Division of
    the French Alternative Energies and Atomic Energy Commission (CEA-DAM).
    CEA, DAM, DIF, F-91297 Arpajon, France
    """

    homepage = 'https://gitlab.com/themys/readers'
    git = 'https://gitlab.com/themys/readers.git'
    url = 'https://gitlab.com/themys/readers/-/archive/0.0.0/readers-0.0.0.tar.gz'

    mainteners = ['billae']
    
    # develop
    version('develop', branch='master', get_full_repo=True)
    # named version
    version('1.2.0', sha256='95ed4756e1496776518562f3fdf998a0c7bfc0733e7a810bec3d706c528f20c2') #f79603b1
    version('1.1.12', sha256='071cf9a5c74b7ac4323c07b5cd13715cf37ee90fa0a5512e626e8c6c5f26aaf9') #08ef6254
    version('1.0.11', sha256='7f5e2c7202061a4d858257df5a1cc9a6b1bfb757341d8ac1f946c61bcf60a77f') #db5cd33e
    version('1.0.10', sha256='b24eca4cbed78d7f6082d980d2ef299490fe1286efeb18c9e87ecef6bb2cbfee') #63c2a07d
    version('1.0.9', sha256='953da6715335759a7ce2f1df7e82c618f092e3480acf00a5d21ef59cd56975c1') #64b86c14
    version('1.0.8', sha256='96e88428f47f8577a89bc4631f644cb8c366844fac67f340536fe19e962cc0ca') #517326e1
    version('1.0.7', sha256='e53ead7e5184432ce9e048c53dd308995f97b4d25f455695904192f400efdcc8') #8a008412
    version('1.0.6', sha256='138eeada4ae58fceaa55db6658a1c9bb0c8998b213e807a089fa433bb9ce303d') #2c2982e9
    version('1.0.5', sha256='6eb2b4e14b4fb95c206c5e325ca09d9982a173b451aecb887d44aac085949afa') #7cbbd120
    version('1.0.4', sha256='75735049b808559fd28f7f900259f8192d4e0b2e4019b08649161689166a2a22') #83568ffa
    version("1.0.3", sha256="2376e0bd60d51d6426a5197e6fdb6b0e29c4ab86e1f6cdbf4cee1774c51e6339") #e4671c25
    version("1.0.2", sha256="67e11482678e8d896b8f69b2440d1a9d1e202d5e404010cdc7acdf8799a5c595") #6634632f
    version('1.0.1', sha256='05402ebc884bf55a9c1b216c0bc2280fa9a911c63daa46729025c00c1f0da8f0') #3fe7dc2f
    version('1.0.0', sha256='b9567d778c50a1c6aef0b8d5ab4f8ce113731797da3f5a9ac63329de0200fc9c') #1a965c6c

    variant('mockhic', default=False, description="Build with MockHIc")
    variant('verbose', default=False, description="Build with verbose")
    variant('debug_for_dev', default=False, description="Build in debug with dev tools")

    depends_on('paraview+python')
    depends_on('mpi')
    depends_on('trompeloeil', when='+debug_for_dev')
    depends_on('trompeloeil@47:', when='@1.1.10:+debug_for_dev')

    #there is a compilation issue if mpich and cmake@3.18.3 is used
    depends_on('cmake@3.21:')

    depends_on('hercule-io')
    depends_on('mockhic', when='+mockhic')
    depends_on('catch2', when='+debug_for_dev')
    depends_on('catch2@3.6:', when='@1.1.10:+debug_for_dev')
    depends_on('py-pre-commit', when='+debug_for_dev')
    depends_on('py-gcovr', when='+debug_for_dev')

    def cmake_args(self):
        args = ['-DCMAKE_CXX_STANDARD=17',
            self.define_from_variant('HIC_MOCKHIC', 'mockhic'),
            self.define_from_variant('READERS_ENABLE_LOGGING', 'verbose'),
            self.define('READERS_VERSION', str(self.spec.version))
        ]
        if '+debug_for_dev' in self.spec:
            args.append('-DBUILD_TESTING=ON')
            args.append('-DENABLE_CODE_COVERAGE=ON')
            args.append('-DCMAKE_BUILD_TYPE=Debug')
        else:
            args.append('-DBUILD_TESTING=OFF')

        return args

    def setup_run_environment(self, env):
        def internal_prepend(pathname):
            try:
                env.prepend_path('PV_PLUGIN_PATH', glob.glob(join_path(self.prefix, 'lib*', pathname))[0])
            except IndexError:
                error_msg = f"IndexError in recipe themys cause {self.prefix}/lib*/{pathname} not found!"
                raise IndexError(error_msg)

        internal_prepend('*/plugins/CEAHerculeReaders')
