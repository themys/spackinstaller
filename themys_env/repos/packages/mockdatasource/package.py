# -*- coding: utf-8 -*-

# Copyright 2020 Comissariat à l'Energie Atomique
#
# This is a template package file for Spack.
# You can install this package by typing:
#
#   spack install mockdatasource
#
# You can edit this file again by typing:
#
#   spack edit mockdatasource
#
# See the Spack documentation for more information on packaging.

from spack.package import *

import glob


class Mockdatasource(CMakePackage):
    """
    This code was developed by Military Applications Division of
    the French Alternative Energies and Atomic Energy Commission (CEA-DAM).
    CEA, DAM, DIF, F-91297 Arpajon, France
    """

    homepage = 'https://gitlab.com/themys/mockdatasource'
    git = 'https://gitlab.com/themys/mockdatasource.git'
    url = 'https://gitlab.com/themys/mockdatasource/-/archive/0.0.0/mockdatasource-0.0.0.tar.gz'

    mainteners = ['billae']
    
    # develop
    version('develop', branch='main', get_full_repo=True)
    # named version
    version('0.1.3', sha256='1dff4f4c2735470324a7fcf50c91cffdf2f753f0d64e577f42d1d47b17e62a51', preferred=True) # 3a1e3e7e
    version('0.1.2', sha256='487e93e074121350b8678bddc1b0d1c4bd4235e8a7cfb681bfa12e20a52646d6') #66832606
    version('0.1.1', sha256='29d40265058f0655845c439fb5dbfa05b74165ce0d98696066c034fe3c768ad4') #528f93df
    version('0.1.0', sha256='d4957186780417311a9890005db220fbc70f94ec2ea7d6f4a15c4553d89fb088') #13ccf714

    variant('mockhic', default=False, description="Build with MockHIc")
    variant('verbose', default=False, description="Build with verbose")
    variant('qt', default=True, description='Enable Qt (gui) support (used for Hercule gui)')
    variant('debug', default=False, description="Build in debug")

    depends_on('paraview+python')

    #there is a compilation issue if mpich and cmake@3.18.3 is used
    depends_on('cmake@3.21:')

    def cmake_args(self):
        args = [
            self.define('CMAKE_CXX_STANDARD', 17),
            self.define_from_variant('HIC_MOCKHIC', 'mockhic'),
            self.define_from_variant('MACRO_READERS_STAT', 'verbose'),
            self.define_from_variant('CMAKE_BUILD_TYPE', 'debug'),
        ]
        return args

    def setup_run_environment(self, env):
        env.prepend_path('PV_PLUGIN_PATH', glob.glob(join_path(self.prefix, 'lib*', '*', 'plugins', 'Mockdatasource'))[0])
