# -*- coding: utf-8 -*-

# Copyright 2020 Comissariat à l'Energie Atomique
#
# This is a template package file for Spack.
# You can install this package by typing:
#
#   spack install themys
#
# You can edit this file again by typing:
#
#   spack edit themys
#
# See the Spack documentation for more information on packaging.

from spack.package import *
import glob


class Themys(CMakePackage):
    """
    Themys is a custom application based on Paraview, it's a ParaView's branded application.
    ParaView is an open-source cross-platform open source data analysis and visualization application
    based on VTK. ParaView and VTK are developped by KitWare.
    The objective of Themys is to offer users a simplified GUI of ParaView adapted,
    while allowing to find at any time the original GUI.
    Ultimately, Themys should offer several simplified GUIs to meet different communities.

    Themys was developed by KitWare under a contract defined with Military Applications Division of
    the French Alternative Energies and Atomic Energy Commission (CEA-DAM).
    CEA, DAM, DIF, F-91297 Arpajon, France
    """
    homepage = 'https://gitlab.com/themys/themys'
    git = 'https://gitlab.com/themys/themys.git'
    url = 'https://gitlab.com/themys/themys/-/archive/0.0.0/themys-0.0.0.tar.gz'

    mainteners = ['billae']
    
    # develop
    version('develop', branch='master', submodules=True, get_full_repo=True)
    # named version
    version('1.1.3', commit='9c7cf2d4', submodules=True)
    version('1.1.2', commit='92e04c8f', submodules=True)
    version('1.1.1', commit='90696d5e', submodules=True)
    version('1.1.0', commit='8e6175fb', submodules=True)
    version('1.0.9', commit='79d3eb18', submodules=True)
    version('1.0.8', commit='7cf250a8', submodules=True)
    version('1.0.7', commit='c31d03eb', submodules=True)
    version('1.0.6', commit='1f795c42', submodules=True)
    version('1.0.5', commit='41122061', submodules=True)
    version('1.0.4', commit='eb083ab6', submodules=True)
    version('1.0.3', commit='fd9055e7', submodules=True)
    version('1.0.2', commit='5fa57508', submodules=True)
    version('1.0.1', commit='9125d183', submodules=True)
    version('1.0.0', commit='98cb93d5', submodules=True)
    version('0.3.3', commit='98e55009', submodules=True)
    version('0.3.2', commit='e1e07496', submodules=True)
    version('0.3.1', commit='3655e5ad', submodules=True)
    version('0.3.0', commit='8517eff8', submodules=True)
    version('0.2.9', commit='176ef8e2', submodules=True)
    version('0.2.8', commit='b1e1fc93', submodules=True)
    version('0.2.6', commit='85ba487f', submodules=True)
    version('0.2.4', commit='93198266', submodules=True)
    version('0.2.3', commit='8f1674c1', submodules=True)
    version('0.2.2', commit='38f4c9e8', submodules=True)
    version('0.2.1', commit='9690de7d', submodules=True)
    version('0.2.0', commit='cabd0314', submodules=True)
    version('0.1.6', sha256='1b864cd381f04bdb2db6624724b8b4df4151d5d586564da1da5ac50ee0bf5142')
    version('0.1.5', sha256='b24f43217a65689a5eba967dd6d777f2ecf31b8ff3cd5fe286d6515feef7a153')
    version('0.1.0', sha256='78248305edcf79e689390e24fc07ac4ccda3858a9ccb6521853508d6fec0091d')
    version('0.0.3', sha256='89598fec82063f6429cea4ab0dea7b22a082e0966b61c6ebff1e9911ec62d92b')

    depends_on('paraview@5.14-themys-1.1.3:', when='@1.1.3:')
    depends_on('paraview@5.14-themys-1.1.2:', when='@1.1.2:')
    depends_on('paraview@5.14-themys-1.1.0:', when='@1.1.0:')
    depends_on('paraview@5.13-themys-1.0.9:', when='@1.0.9:')
    depends_on('paraview@5.13-themys-1.0.8:', when='@1.0.8:')
    depends_on('paraview@5.12-themys-1.0.7:', when='@1.0.7:')
    depends_on('paraview@5.12-themys-1.0.6:', when='@1.0.6:')
    depends_on('paraview@5.12-themys-1.0.4:', when='@1.0.4:')
    depends_on('paraview@5.12-themys-1.0.3:', when='@1.0.3:')
    depends_on('paraview@5.12-themys-1.0.2:', when='@1.0.2:')
    depends_on('paraview@5.12-themys-1.0.0:', when='@1.0.0:')
    depends_on('paraview@5.11-themys-0.3.3:', when='@0.3.3:')
    depends_on('paraview@5.11-themys-0.3.2:', when='@0.3.2:')
    depends_on('paraview@5.11-themys-0.3.1:', when='@0.3.1:')
    depends_on('paraview@5.11-themys-0.3.0:', when='@0.3.0:')
    depends_on('paraview@5.11-themys-0.2.9:', when='@0.2.9:')
    depends_on('paraview@5.11-themys-0.2.8:', when='@0.2.8:')
    depends_on('paraview@5.11-themys-0.2.6:', when='@0.2.6:')
    depends_on('paraview@5.11-themys-0.2.4:', when='@0.2.4:')
    depends_on('paraview@5.11-themys-0.2.3:', when='@0.2.3:')
    depends_on('paraview@5.11-themys-0.2.2:', when='@0.2.2:')
    depends_on('paraview@5.10-themys-0.2.0:', when='@0.2.1:')
    depends_on('paraview@5.10-themys-0.2.0:', when='@0.2.0:')
    depends_on('paraview@5.10-themys-0.1.6:', when='@0.1.6:')
    depends_on('paraview@5.10-themys-0.1.5:', when='@0.1.5:')
    depends_on('paraview@5.9.0:', when='@0.1.0')
    depends_on('py-sphinx', when='@1.0.4:')

    depends_on('paraview+python+qt')

    def cmake_args(self):
        args = ['-DCMAKE_CXX_STANDARD=17', '-DBUILD_DOCUMENTATION=ON']

        return args


    def setup_run_environment(self, env):

        def internal_prepend(pathname):
            try:
                env.prepend_path('PV_PLUGIN_PATH', glob.glob(join_path(self.prefix, 'lib*', pathname))[0])
            except IndexError:
                error_msg = f"IndexError in recipe themys cause {self.prefix}/lib*/{pathname} not found!"
                raise IndexError(error_msg)
        internal_prepend('themys')
        if self.spec.version > Version('1.0.0'):
            internal_prepend('PluginsPython')
