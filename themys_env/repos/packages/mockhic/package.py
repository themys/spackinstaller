# -*- coding: utf-8 -*-
# Copyright 2020 Comissariat à l'Energie Atomique
#
# This is a template package file for Spack.
# You can install this package by typing:
#
#   spack install pluginparaviewhercule
#
# You can edit this file again by typing:
#
#   spack edit pluginparaviewhercule
#
# See the Spack documentation for more information on packaging.

from spack.package import *

class Mockhic(CMakePackage):
    """
    This code was developed by Military Applications Division of
    the French Alternative Energies and Atomic Energy Commission (CEA-DAM).
    CEA, DAM, DIF, F-91297 Arpajon, France
    """

    homepage = 'https://gitlab.com/themys/mockhic'
    git = 'https://gitlab.com/themys/mockhic.git'
    url = 'https://gitlab.com/themys/mockhic/-/archive/0.0.0/mockhic-0.0.0.tar.gz'

    mainteners = ['billae']
    
    # develop
    version('develop', branch='master')
    # named version
    version('0.1.1', sha256='f978739957df4f9d3de70dae50807e893234f95c147bd9b646aafa81fe51d4b5', preferred=True) # fd25684d
    version('0.1.0', sha256='29d780c3cbb2a4ea1272097cf5a31103dd0cde7dbf924d51e182777b17866072')

    def cmake_args(self):
        args = ['DCMAKE_CXX_STANDARD=17', ]
        return args
