# -*- coding: utf-8 -*-

# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
#
# This is a template package file for Spack.
#
#     spack install serverplugin
#
# You can edit this file again by typing:
#
#     spack edit serverplugin
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack.package import *
from spack import *
import glob


class Serverplugins(CMakePackage):
    """This is tthe server side plugin for Themys."""

    homepage = 'https://gitlab.com/themys/themysserverplugins'
    git = 'https://gitlab.com/themys/themysserverplugins.git'
    url = 'https://gitlab.com/themys/themysserverplugins/-/archive/0.0.0/themysserverplugins-0.0.0.tar.gz'

    mainteners = ['billae']

    # develop
    version('develop', branch='main')
    # named version

    version('1.1.3', sha256='f0ec127cc0a219121d6e65f0fc553f03ba1e5843db2394e9a294d3ad32ce18e9') #4cbb824c
    version('1.1.2', sha256='25778925825d283e19f6d4d714a4a16e5eff07f4a0d13a660e4a73bbd97568c7') #8c6edd72
    version('1.1.1', sha256='6c24ef20a7f19b68ad041f59ad41886cb7d3d9c26a50b595f1e93976a5df5fbd') #b9399b62
    version('1.1.0', sha256='9546141b9c9acf5101bb4515f24f63d000edc2c377f63bfa8ae3787212c6e825') #8f2bd5c5
    version('1.0.9', sha256='665ddcf01cd37d2fac29a2e350a532e31df290f1ec6d78af860a4853bdd44f75') #68a4dbe6
    version('1.0.8', sha256='300263d00b4d74b3c22a04002d8b5884f19ea9cf6e4502c2a9ea65e115dd4205') #93baf5bc
    version('1.0.7', sha256='1cd6164f5a4f05db1114292b83043c5ff955ff930b5da3686a5d35817486746e') #faf3c9cc
    version('1.0.6', sha256='5bb91a22fccec39149ccd807650571fc43c75161d8917cebd00bc47c7b455a7a') #4c6a1f99
    version('1.0.5', sha256='3cf06a1f079c2e7d5121789db3579ee201220c0dd0a46eacd656026964f3df52') #148128ce
    version('1.0.4', sha256='2418dd33fedc835e75ab660dcc2064328496e823af89790299d7dee5bc6b0515') #0512c358
    version('1.0.3', sha256='703077f23aee744c19620ffc5278ff99cf47c71461966d009604c28585786577') #ce6876b7
    version('1.0.2', sha256='87e991f0c962b8f1801ab7ad1adb554c29a2442233c94aa1a8b7b327aab8ff66')
    version('1.0.1', sha256='73db40191b7c69b6af4f7b48c46cbf471c7e32c1a82e0ee075cc8ae29064d351')
    version('1.0.0', sha256='7f50bc2909a3d37ffc528d41ad24842d001f19a6c456709b32bb6ee5e80bb073')
    version('0.3.3', sha256='31a3af0f1e14deb1defcc6e8c9c1945d913ac88b4b31d79cc8d04d842dfeb395')
    version('0.3.2', sha256='3cdbc229508bad5864bc9db15be61f3f84d1b27f6943662b64e8fc9c75df08d5')
    version('0.3.1', sha256='4251963284a7c608747041523376c1c83cf45226ca5717347411a8d8bd6a9c90')
    version('0.3.0', sha256='5c83a5ae5ab2d4031e354af4616b59789e5c3ffa20b74177a310194c75eb0fa1')
    version('0.2.9', sha256='ad9e7f21c7407adbc181562b76842dad37ba57ba18c25acf435da3ff73ca5c83')
    version('0.2.8', sha256='7d9fd6e7da458e97ffea8cde6f2ee49b48675229cf78ee67d3bd80e5fd43c33a')
    version('0.2.6', sha256='ef5ed1bd52efc738d73ca6213e913bbb3eec6e13871a2cf5f20998a3f1ba213d')
    version('0.2.4', sha256='da99bc5f8444198303b258e127a71956a0fb74dc8a085298844ae5f17d14479b')
    version('0.2.3', sha256='2bad91c8d1a1a86c38cc0d57bcdf73903a9cd66b85bfa3ebbb1cb97e12c4dcdd')
    version('0.2.2', sha256='43b3cbb05b39524cc1f8f8b6a36f33fa9a560b889361fccfb9b2482e1194478b')
    version('0.2.1', sha256='6c6d2f8879cc8ec4abf0ad1257a3f073badf2fc615fbf9a676b2709803623ad1')
    version('0.2.0', sha256='de107b9db59409582495f7b6c111da21e42cb381686b1984ece1b6a7224f9554')

    depends_on('paraview@5.14-themys-1.1.3:', when='@1.1.3:')
    depends_on('paraview@5.14-themys-1.1.2:', when='@1.1.2:')
    depends_on('paraview@5.14-themys-1.1.0:', when='@1.1.0:')
    depends_on('paraview@5.13-themys-1.0.9:', when='@1.0.9:')
    depends_on('paraview@5.13-themys-1.0.8:', when='@1.0.8:')
    depends_on('paraview@5.12-themys-1.0.7:', when='@1.0.7:')
    depends_on('paraview@5.12-themys-1.0.6:', when='@1.0.6:')
    depends_on('paraview@5.12-themys-1.0.4:', when='@1.0.4:')
    depends_on('paraview@5.12-themys-1.0.3:', when='@1.0.3:')
    depends_on('paraview@5.12-themys-1.0.0:', when='@1.0.0')
    depends_on('paraview@5.11-themys-0.3.3:', when='@0.3.3')
    depends_on('paraview@5.11-themys-0.3.2:', when='@0.3.2')
    depends_on('paraview@5.11-themys-0.3.0:', when='@0.3.0')
    depends_on('paraview@5.11-themys-0.2.9:', when='@0.2.9')
    depends_on('paraview@5.11-themys-0.2.8:', when='@0.2.8')
    depends_on('paraview@5.11-themys-0.2.6:', when='@0.2.6')
    depends_on('paraview@5.11-themys-0.2.4:', when='@0.2.4')
    depends_on('paraview@5.11-themys-0.2.3:', when='@0.2.3')
    depends_on('paraview@5.11-themys-0.2.2:', when='@0.2.2')
    depends_on('paraview@5.10-themys-0.2.0:', when='@0.2.1')
    depends_on('paraview@5.10-themys-0.2.0:', when='@0.2.0')
    depends_on('paraview+python')

    def cmake_args(self):
        args = ['-DCMAKE_CXX_STANDARD=17', ]

        return args


    def setup_run_environment(self, env):
        def internal_prepend(pathname):
            try:
                env.prepend_path('PV_PLUGIN_PATH', glob.glob(join_path(self.prefix, 'lib*', pathname))[0])
            except IndexError:
                error_msg = f"IndexError in recipe themys cause {self.prefix}/lib*/{pathname} not found!"
                raise IndexError(error_msg)

        internal_prepend('themys')
        if self.spec.version > Version('1.0.0'):
            internal_prepend('PluginsPython')
