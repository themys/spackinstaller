#!/bin/bash

#This script will generate a spack.yaml fiile in the themys_env directory, in argument, by changing all variables defined in the file env_inc.ini also in argument

if [[ $# != 2 ]] ; then
    echo 'missing argument: please give a environment directory and a env_inc file'
    exit 1
fi

readonly ENV_DIR=$1
readonly INI_FILE=$2

. ${INI_FILE}

cat << EOF > ${ENV_DIR}/spack.yaml 
spack:
${_INC_}
  concretizer:
    unify: true
  view: false
${_PKG_}
  modules:
    default:
      roots:
        tcl: ${_MODULEDIR_}/modules
      tcl:
        projections:
          all: '{name}-{version}'
        hash_length: 0
        all:
          autoload: direct
      enable:
      - tcl
  repos:
  - \${SPACK_ENV}/repos
${_REPOS_}
  mirrors:
    env_mirror: file://\${SPACK_ENV}/mirrors
${_MIRRORS_}
  specs: ${_SPEC_}
EOF
