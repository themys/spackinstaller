
Ce projet est exclusivement conçu pour répondre aux besoins d'installation (interne et externe) de la suite logicielle Themys.
Il contient l'ensemble des recettes **Spack** associées aux projets du groupe Themys, ainsi que des aides au déploiement automatique de ceux-ci (scripts, fichiers de configurations, etc).


# Pré-requis d'installation

Avant d'installer la suite logielle Themys via les outils de ce projet *Spack Installer*, il est nécessaire de satisfaire les prérequis suivants:

Notes: Un script (```fixup_system_prerequisites.sh```) modifiant votre environnement pour satisfaire ces prérequis est à disposition.

## git et git-lfs
Pour que l'installation soit correcte, il faut avoir installé git et git-lfs.

git-lfs se télécharge sur le site [github git-lfs](https://git-lfs.github.com/). Le fichier archive doit être détarré dans le home avant de lancer la commande install.sh présente dans ce dernier. L'installation se termine par un :
```sh
git lfs install
```
L'installation peut être validé en regardant le contenu du fichier ~/.gitconfig

## Contraintes Système
Lors des premières installations, nous avons constaté une hétérogénéité
des configurations machines. Afin de ne rencontrer aucune entrave à l'installation
lié à cela, nous conseillons :
- de disposer de 4Go mémoire par core/hyperthread, swap confondu.
[Procédure d'extension du swap sur ubuntu](https://bogdancornianu.com/change-swap-size-in-ubuntu/)
- de disposer d'un espace ```/tmp``` conséquent car l'installation par **Spack**
passe par un build sur le ```/tmp```, nous conseillons au moins 16Go.
- de disposer d'une gestion des modules si vous souhaitez utiliser ceux générés (sans cela une erreur lié à *module* peut apparaitre mais est sans conséquence si vous n'utilisez pas les modules).


[[_TOC_]]

# Installation

Le déploiement de la suite logicielle Themys se fait via l'utilisation du script ```deploy.sh```.

Il est d'abord nécessaire de modifier le fichier d'initialisation de la configuration
de l'installation ```env_inc.ini``` (contenu dans le dossier ```external```) afin
de l'adapter en fonction de son environnement de travail.

Il est ensuite possible de lancer différents type d'installations: 

Par défaut se lance l'installation des versions de releases (deploy sans options), mais il est possible de demander le mode développement (option -d et -r).

```
 ./deploy -h
Usage : deploy [OPTION]... [FILE]
   Generation of themys project through spack package manager

Options:
   -h: prints this message
   -d: enable the developer mode. The directories (readers themys paraview) 
       will be cloned inside the themys_env spack environment.
       Should be run only once to start the themys install or in 
       combinaison with the -f (force) option.
   -f: enable the force mode. If the themys project has been generated once 
       in developer mode, a second call to the developer mode will failed in 
       order to avoid the lose of local modifications made inside the 
       directories (readers themys paraview).
       The force mode will override this verification thus leading to the removal 
       of those directories before their re-creation.
       This options has sense only in combinaison with the -d (develop) option.
   -r: enable the redevelop mode. It is the same as developer mode except 
       that no modification is made among the directories (readers themys paraview).
       The developer mode should have been activated at 
       least once before running the redevelop mode
File:
   The path toward the env_inc.ini file (external/env_inc.ini by default)

```

## Modification du fichier de configuration
Afin de générer un fichier d'environnement compatible avec le contexte, il est
nécessaire de remplir manuellement les valeurs dans le fichier ```env_inc.ini```
(contenu dans le dossier ```external```). Celles-ci seront ainsi automatiquement
utilisées lors de la création de l'environnemnt **Spack**.

Les paramètres à renseigner sont les suivants:

	- _INC_: "  include: [<liste de fichiers à inclure dans l'environnement>]"
	- _COMPILER_: "<compilateur>@<version>"
	- _GROUP_: "<nom du group UNIX>"
   	- _MODULEDIR_ : "<chemin vers l'installation des modules>"
   	- _REPOS_ : "  - <chemin vers une repository spack>"
   	- _MIRRORS_ : "<nom du mirroir>: <type>:<adresse ou chemin du mirroir>"
    - _SPEC_: "[<ensemble des produits finaux à installer>]"
    - _PKG_ : "  packages: [<configuration spack de certains paquet si besoin>]"

> ATTENTION :
> - Les espaces dans la liste ci-dessus sont importants pour la syntaxe
> du fichier ***spack.yaml***.
> - Des chemins d'accès peuvent être donnés mais relativement au
> répertoire de l'environnement spack (ici, t*hemys_env*).


### Utiliser le openGL systeme pour prendre en compte sa carte graphique avec Spack:

Suivant si une carte graphique est disponible, il est possible d'indiquer à Spack d'utiliser OSmesa ou GLX pour implementer l'API OpenGL.

Si aucune carte graphique n'est disponible, Spack fera le choix de OSmesa par défaut.
Afin de prendre en compte le GLX systeme pour utiliser une carte graphique, il faut le spécifier dans spack en tant qu'*external*, dans la partie ```_PKG_``` du fichier ```env_inc.ini```:

```
_PKG_="  packages:
    libglx:
      require: [opengl]
    opengl:
      buildable: false
      externals:
      - prefix: /usr/
        spec: opengl@4.6"
```

Il est à noter que le champs "prefix" doit correspondre à la racine des bibliothèques et des entêtes : utiliser /usr et non le chemin vers la bibliothèque.
Pour savoir quelle est la version de GLX à mettre dans "spec", utiliser ``cd /usr/include/GL && grep -Ri gl_version``.

## Description des étapes suite au lancement du script deploy
La procédure exécutée par le script ```deploy``` est la suivante:

+ Cloner un projet **Spack**, si il n'existe pas ;
+ Initialiser **Spack**, affiche sa version ;
+ Génèrer le fichier d'environnemnt **Spack** à partir du fichier ```env_inc.ini```;
+ Valider le compilateur demandé (compiler@version) :
    - Vérifier son existence ;
    - Vérifier qu'il comprend les languages : *cc cxx f77 fc* ;
+ Installer l'environnement **Spack** décrit dans ```themys_env/spack.yaml``` :
    - Activer l'environnement correspondant ;
    - Effectuer la concrétisation **Spack** ;
    - Installer les produits ;
    - Générer les modules dans le chemin spécifié pour **\_MODULEDIR\_**.
  

# Remarques
A l'issu de cette production, on constate :
- La création d'un répertoire ```spack``` contenant :
    - **Spack** (clonée au début de la procédure) ;
    - L'ensemble des produits installés (dans ```spack/opt/...```) ;
- La création d'un fichier ```spack.lock``` dans ```themys_env``` qui décrit les versions utilisées de chacun des produits installés pour l'environnement ;
- La création d'un répertoire ```modules``` dans **\_MODULEDIR\_**  contenant les modules associès à chacun des produits installés pour l'environnement. Chaque module inclut le chargement des modules nécessaires à son exécution, ainsi il suffit d'en charger qu'un pour un produit final.
- La création de dossiers de dévellopement dans ```themys_env``` si le script ```deploy``` a été lancé avec l'option développeur (```-d``` ou ```-r```). Ces dossiers contiennent les sources des produits modifiables qui seront utilisé lors de la procédure d'installation.
