# Copyright 2013-2018 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *
from spack import *
import glob

try:
    from spack.pkg.spack_organizer.debuggabler import apply_debuggabler_pass
except ModuleNotFoundError:
    def apply_debuggabler_pass(*args, **kwargs):
        """No debuggabler."""
        pass


class HerculeCommunity(CMakePackage):
    """Hercule libraries, bindings, and tools"""

    homepage = 'https://gitlab.com/hercule-io/public/hercule'
    git = 'https://gitlab.com/hercule-io/public/hercule.git'
    url = 'https://gitlab.com/hercule-io/public/hercule/-/archive/0.0.0/hercule-0.0.0.tar.gz'

    # version('develop', branch='master')
    version('2.4.5.3_rc6_ext', sha256='2b29afc0095c497a4982dac7f202ce0e2969f786d1114dec56abae491c74b6ff', preferred=True)
    version('2.4.4', sha256='b4ad9c0d38018ab8ad4c36dd5ca0b681')


    provides('hercule-io')

    variant('default-use-old-format', default=True, description='Build to use default old format')
    variant('shared', default=True, description='Build dynamic shared libraries')

    variant('openmp', default=False, description='Build OpenMP variants of libs/bins (suffixed "-openmp")')
    variant('mpi', default=False, description='Build MPI variants of libs/bins (suffixed "-mpi")')

    variant('c', default=False, description='Build C bindings')
    variant('fortran', default=False, description='Build Fortran bindings')
    variant('python', default=False, description='Build Python bindings')

    variant('examples', default=False, description="Build examples/tests")
    variant('tools', default=False, description="Build tools")
    variant('qt', default=False, description="Build QT tools")

    #variant('ccc', default=False, description="Build with ccc cmd")
    #variant('ccc-user', default=False, description="Build with ccc-user")
    #variant('ccc-hsm', default=False, description="Build with ccc-hsm")


    variant('test', default=False, description="Build tests")
    variant('doc', default=False, description="Build doxygen doc")


    # Note: `depends_on`'s `type` default is ('build', 'link'),
    # `type=all` is ('build', 'link', 'run', 'test')


    depends_on('swig', type='build')  # when='+cs'
    depends_on('boost@1.66.0:', type=('build', 'link'))
    depends_on('libtirpc')

    depends_on('mpi', when='+mpi', type=all)
    depends_on('qt@5:', when='+tools+qt')

    depends_on('python', when='~python', type=('build'))
    depends_on('python', when='+python', type=all)
    depends_on('py-pybind11@2.3.0:', when='+python', type=('build', 'link'))
    depends_on('py-mpi4py', when='+python+mpi', type=all)
    depends_on('py-numpy', when='+python+test', type=('run', 'test'))
    extends('python', when='+python')


    #depends_on('ccc-user', when='+ccc-user')
    #depends_on('ccc-hsm', when='+ccc-hsm')
    #depends_on('cmake-find-ccc', when='+ceadeps+ccc-user')
    #depends_on('cmake-find-ccc', when='+ceadeps+ccc-hsm')

    depends_on('doxygen+graphviz', when='+doc', type='build')

    patch("adds_o_option_to_swig.patch", when="@:2.4.5.3_rc6_ext")
    patch("fix_gmev2_col_log_buffer_overflow.patch", when="@:2.4.5.3_rc6_ext")
    patch("fix_lm_dns_get_domaine_buffer_overflow.patch", when="@:2.4.5.3_rc6_ext")

    apply_debuggabler_pass(default_source=False)

    def url_version(self, version):
        """Transform clean spack version into Hercule tag.

        Hercule tags looks like "v2.4.0_rc4". A spack-friendly version looks
        more like "2.4.0-rc4".

        """
        v = str(version)
        if v.startswith('v'):
            return v
        else:
            return 'v' + v.replace('-rc', '_rc')

    def setup_dependent_environment(self, spack_env, run_env, dependent_spec):
        # Set CMAKE_PREFIX_PATH for other packages that `depends_on('hercule')`
        spack_env.prepend_path('CMAKE_PREFIX_PATH', self.prefix)

    def cmake_args(self):
        args = [
            self.define_from_variant('BUILD_SHARED_LIBS', 'shared'),

            # Don't use "easy" way, selectively set BUNDLE below
            self.define('HERCULE_EASY_DEPS', False),
        ]

        # If we are not using ceadeps, use hercule's bundled versions
        use_bundled = '+ceadeps' not in self.spec
        for pkg in ['MachineTypes', 'MachineIo64', 'Lm', 'Gme']:
            args.append(
                self.define('HERCULE_BUNDLE_{}'.format(pkg), use_bundled))

        # Those are translatable from spack variant to cmake -DHERCULE_ENABLE_
        ENABLABLES = [
            # 'noopenmp',
            'openmp',
            'mpi',
            'fortran',
            'c',
            'python',
            # 'c-mpi',
            # 'fortran-mpi',
            # 'python-mpi',
            # 'services_cs',
            # 'ic_cs',
            # 'cs',
            # 'services',
            'tools',
            'examples',
            'default-use-old-format',
            #'ccc',
            #'ccc-user',
            #'ccc-hsm',
            'qt',
            # 'vtk',
            # 'netcdf',
            'doc',
            # 'data-model-nstructured',
            # 'data-model-structured',
            # 'data-model-amr',
            # 'data-model-molecule',
            # 'data-model-laser',
        ]
        for en in ENABLABLES:
            variant = en
            cmake_var = 'HERCULE_ENABLE_' + en.replace("-", "_").upper()
            args.append(self.define_from_variant(cmake_var, variant))

        # Fix cmake taking python3 even if `which python` is python2
        if '+python' in self.spec:
            args.append("-DPYTHON_EXECUTABLE:FILEPATH=" +
                        self.spec['python'].command.path)

        modules = self.compiler.modules or []
        for dep in self.spec.traverse(deptype=('run', 'link')):
            modules.extend(dep.external_modules or [])
        if modules:
            module_cmd = ('module purge >&/dev/null || : ; module load {} ;'
                          .format(' '.join(modules)))
            args.extend([
                self.define("HERCULE_MODULE_LOAD_MPI_CMD", module_cmd),
                self.define("HERCULE_MODULE_LOAD_SEQ_CMD", module_cmd),
                self.define("HERCULE_MODULE_LOAD_QT_CMD", module_cmd),
            ])
            # Self-test modules
            bash = which('bash')
            _ = bash('-c', 'set -x; set -e;\n{}\necho OK'.format(module_cmd),
                     output=str, error=str)
        return args

    def setup_run_environment(self, env):
       env.prepend_path('PYTHONPATH', glob.glob(join_path(self.prefix, 'lib*', 'python*', 'site-packages'))[0])
